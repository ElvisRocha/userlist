package elvis.rocha.user.list.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity{
    private ListView lvUsers;
    List<UserModel> userModelsList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvUsers = (ListView) findViewById(R.id.lvUsers);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.github.com")
                .build();

        UserService service = restAdapter.create(UserService.class);

        service.getUser(new Callback<List<UserModel>>() {
            @Override
            public void success(List<UserModel> userModels, Response response) {
                UserAdapter adapter = new UserAdapter(MainActivity.this, userModels);
                userModelsList = userModels;
                lvUsers.setAdapter(adapter);
        }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(MainActivity.this, retrofitError.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                int idUser =  userModelsList.get(position).getId();
                String login =  userModelsList.get(position).getLogin();
                String userImage =  userModelsList.get(position).getAvatar_url();
                UserDetailsActivityCall(idUser, login, userImage);
            }
        });
    }

    public void UserDetailsActivityCall(int idUser, String login, String userImage){

        Intent intent = new Intent(this, User_Details_Activity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("login", login);
        intent.putExtra("image", userImage);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


