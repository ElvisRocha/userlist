package elvis.rocha.user.list.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;


public class User_Details_Activity extends AppCompatActivity {
    TextView tvLogin;
    TextView tvUserId;
    ImageView ivUserImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvUserId = (TextView) findViewById(R.id.tvUserId);
        ivUserImage = (ImageView) findViewById(R.id.ivUserImage);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null){
            int idUser = (int)bundle.get("idUser");
            String login = (String)bundle.get("login");
            String Image = (String)bundle.get("image");
            tvUserId.setText(String.valueOf(idUser));
            tvLogin.setText(login);
            Glide.with(this)
                    .load(Image)
                    .into(ivUserImage);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
