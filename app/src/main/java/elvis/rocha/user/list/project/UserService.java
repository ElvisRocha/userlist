package elvis.rocha.user.list.project;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by elvis on 25/02/2016.
 */
public interface UserService {
    @GET("/users")
    void getUser(Callback<List<UserModel>> callback);
}
