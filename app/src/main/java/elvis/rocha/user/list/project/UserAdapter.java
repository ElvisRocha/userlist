package elvis.rocha.user.list.project;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by elvis on 26/02/2016.
 */
public class UserAdapter extends ArrayAdapter<UserModel> {
    Context myContext;
    private List<UserModel> userList;

    public UserAdapter(Context context, List<UserModel> users){
        super(context, R.layout.listitem, users);
        this.myContext = context;
        userList = users;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = ((Activity)myContext).getLayoutInflater().from(getContext());

        View item = inflater.inflate(R.layout.listitem, null);

        TextView txtUserId = (TextView) item.findViewById(R.id.tvUserId);
        txtUserId.setText(String.valueOf(userList.get(position).getId()));

        TextView txtLogin = (TextView) item.findViewById(R.id.tvLogin);
        txtLogin.setText(userList.get(position).getLogin());

        return item;
    }
}
